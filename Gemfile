source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Core
gem 'rails', '~> 5.0.2'
gem 'mysql2', '>= 0.3.18', '< 0.5'

# Puma as the app server
gem 'puma', '~> 3.0'

# Configurations
gem 'config', '~> 1.2.1'

# Assets
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'bootstrap-sass', '~> 3.3.7'

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Authentication
gem 'devise', '~>4.2.1'

# Externals
gem 'dropbox_api', '~> 0.1.5'
gem 'wunderlist-api', '~> 1.1.2'
gem 'omniauth-wunderlist',  '~> 0.0.2'

group :development, :test do
  gem 'byebug', platform: :mri
end

group :development do
  gem 'better_errors',          '~> 1.1.0'
  gem 'binding_of_caller'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end