Rails.application.config.middleware.use OmniAuth::Builder do
  provider :wunderlist, Settings.wunderlist.app_id, Settings.wunderlist.app_secret
end