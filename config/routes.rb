Rails.application.routes.draw do
  devise_for :users
  root to: "home#index"
  resources :users
  resources :dropbox, only: [:index]

  controller 'dropbox' do
    get 'dropbox/file_download/*path' => 'dropbox#file_download', as: 'file_download', constraints: { path: /.*/ }
    get 'dropbox/auth' => 'dropbox#auth'
    get 'dropbox/auth_callback' => 'dropbox#auth_callback'
    get 'dropbox/disconnect' => 'dropbox#disconnect'
  end

  resources :wunderlist, only: [:index]

  get 'wunderlist/auth' => 'wunderlist#auth'
  get 'wunderlist/auth_callback' => 'wunderlist#auth_callback'
  get 'wunderlist/disconnect' => 'wunderlist#disconnect'
end
