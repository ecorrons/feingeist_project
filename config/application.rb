require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module FeingeistProject
  class Application < Rails::Application

    config.time_zone = 'Berlin' 
    config.autoload_paths+= Dir["#{config.root}/app/models/**"]

    config.encoding = 'utf-8'
  end
end