class DropboxController < ApplicationController
  before_action :authenticate_user!

  # GET /dropbox/auth_callback?code=SoMeToKeN
  def auth_callback
    begin
      auth_bearer = authenticator.get_token(params[:code], redirect_uri: redirect_uri)
      token = auth_bearer.token

      DropboxWrapper.persist_token!(user: current_user, token: token)

      flash[:notice] = "Your dropbox account has been connected successfully"
    rescue => err
      logger.error err.message
      flash[:error] = "Your dropbox account could not be connected"
    end

    redirect_to dropbox_index_path
  end

  def file_download
    # TODO Create a singleton of client so I don't have to instanciate it every time
    client = DropboxWrapper.new(token: token)
    file_contents = client.render_file path: params[:path]

    send_data file_contents, filename: params[:path], disposition: 'attachment'
  end

  private

  def auth_url
    authenticator.authorize_url(redirect_uri: redirect_uri)
  end

  def authenticator
    DropboxApi::Authenticator.new(Settings.dropbox.app_id, Settings.dropbox.app_secret)
  end

  def redirect_uri
    Settings.dropbox.app_auth_callback_url
  end
end