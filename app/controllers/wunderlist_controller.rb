class WunderlistController < ApplicationController
  before_action :authenticate_user!

  # GET /wunderlist/auth_callback?state=SoMeRanDomStriNg&code=SoMeCoDe
  def auth_callback
  # TODO consider params[:state]; if it don't match request was created by a third party and the process should abort.
    conn = Faraday.new(url: authenticator.site) do |faraday|
      faraday.request  :url_encoded
      faraday.response :logger
      faraday.adapter  Faraday.default_adapter
    end

    response = conn.post authenticator.token_url, { code: params[:code],
                                                    client_secret: Settings.wunderlist.app_secret,
                                                    client_id: Settings.wunderlist.app_id }
    parsed_response = JSON::parse response.body
    WunderlistWrapper.persist_token!(user: current_user, token: parsed_response["access_token"])

    redirect_to wunderlist_index_path, notice: "Your wunderlist account has been connected successfully"
  end


  private

  def auth_url
    # Wunderlist auth expects a random string
    random = WunderlistWrapper.generate_random

    "#{authenticator.authorize_url}?client_id=#{Settings.wunderlist.app_id}&redirect_uri=#{redirect_uri}&state=#{random}"
  end

  def authenticator
    OmniAuth::Strategies::Wunderlist.new({}).options.client_options
  end

  def redirect_uri
    Settings.wunderlist.app_auth_callback_url
  end
end