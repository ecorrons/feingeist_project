class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  # GET /<service>/auth
  def auth
    if current_user.authenticated_with? (controller_name.to_sym)
      render :index
    else
      redirect_to auth_url
    end
  end

  def index
    @client = "#{controller_name}Wrapper".classify.constantize.new(token: token) if current_user.authenticated_with? (controller_name.to_sym)
  end

  def disconnect
    TokenUser.destroy_all(user: current_user, token_type: TokenUser.token_types[controller_name.to_sym])

    redirect_to send("#{controller_name}_index_path"), alert: "Your #{controller_name} account has been disconnected"
  end

  protected

  def token
    TokenUser.find_by(user: current_user, token_type: TokenUser.token_types[controller_name.to_sym]).try(:token)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

end