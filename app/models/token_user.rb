class TokenUser < ApplicationRecord
  belongs_to :user

  # This enum should add a new token type when we add a new service support
  enum token_type: [ :dropbox, :wunderlist ]
end