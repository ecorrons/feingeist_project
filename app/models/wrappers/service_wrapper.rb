class ServiceWrapper

  def object
    @client
  end

  def self.persist_token!(user: user, token: token, token_type: token_type)
    # Persist token for user
    TokenUser.create!(user: user, token: token, token_type: TokenUser.token_types[self::NAME])
  end
end