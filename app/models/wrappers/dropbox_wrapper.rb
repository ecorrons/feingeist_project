class DropboxWrapper < ServiceWrapper

  NAME = :dropbox

  def initialize(token: token)
    @client = DropboxApi::Client.new(token)
  end

  def entries(route: "")
    @result = @client.list_folder route

    @result.entries
  end

  def item_type entry
    entry.is_a?(DropboxApi::Metadata::Folder) ? 'folder' : 'file'
  end

  def name
    @client.get_current_account.name.display_name
  end

  def render_file(path: path)
    file_contents = ""
    @client.download("/#{path}") do |chunk|
      file_contents << chunk
    end

    file_contents
  end
end