class WunderlistWrapper < ServiceWrapper

  NAME = :wunderlist

  def initialize(token: token)
    @client = Wunderlist::API.new({client_id: Settings.wunderlist.app_id, access_token: token})
  end

  def self.generate_random
    (0...50).map { ('a'..'z').to_a[rand(26)] }.join
  end

  def name
    @client.user.name
  end
end