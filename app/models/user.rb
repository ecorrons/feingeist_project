class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

  has_many :token_users

  def authenticated_with? service_name
    TokenUser.find_by(user: self, token_type: TokenUser.token_types[service_name]).try(:token).present?
  end
end