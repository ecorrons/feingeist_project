class CreateTokenUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :token_users do |t|
      t.string :token
      t.references :user, foreign_key: true
      t.integer :token_type

      t.timestamps
    end
  end
end
