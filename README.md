# README #

This is Feingeist Homework :)

### Install ###

* Checkout repo
* `cp config/database.sample.yml config/database.yml`
* `cp config/settings.sample.yml config/settings.yml`
* Replace every `#TOKEN#` in those files with the correct information.
* `bundle exec rake db:create db:migrate db:seed` 

### Services involved###

It's compulsory to make the app work to have applications at Dropbox Developer and Wunderlist Developers.
In the case of Dropbox I configured my test app with the 'folder permission' option (a special folder for the application is created, and the user can only read/write on that folder).
 
### Who do I talk to? ###

* María Emilia Corrons <maemco4@gmail.com>